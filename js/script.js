window.onscroll = function () { myFunction() };

var sticky;
//SCROLL
$(function () {
    //doScroll();
    loadChampionsLeague();
    loadPremierLeague();
    loadNationalLeagueDE();
    initNavBar();
    loadRecentMatchesDE();
    loadRecentMatchesNLD();
});

function initNavBar() {
    var navbar = document.getElementById("navbar");
    sticky = navbar.offsetTop;
}

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}

function doScroll() {
    $("a[href=#]").on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500, 'linear');
    });
}

function doIt(founded) {
    alert("Founded: " + founded);
}
function loadChampionsLeague() {
    $("#result1").click(function () {
        $.ajax({
            method: "GET",
            headers: { "X-Auth-Token": "0da2f2ae0195484484340705e7d5bf4f" },
            url: "https://api.football-data.org/v2/competitions/2001/teams/",
            success: function (data) {
                console.log(data);
                $("#table").html(data.teams);

                var output = "";
                /*
                data.teams.forEach(element => {
                    console.log(element.name);
                });
                */
                for (var i = 0; i < data.teams.length; i++) {
                    console.log(data.teams[i]);
                    output += "<tr  onClick='doIt(" + data.teams[i].founded + ")'><td>" + data.teams[i].tla + "</td><td>" + data.teams[i].name + "</td><td>" + data.teams[i].website + "</td></tr>";
                }
                console.log(output);
                $("#table").html(output);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
}
function loadPremierLeague() {
    $("#result2").click(function () {
        $.ajax({
            method: "GET",
            headers: { "X-Auth-Token": "0da2f2ae0195484484340705e7d5bf4f" },
            url: "https://api.football-data.org/v2/competitions/2021/teams",
            success: function (data) {
                console.log(data);
                $("#table").html(data.teams);

                var output = "";
                /*
                data.teams.forEach(element => {
                    console.log(element.name);
                });
                */
                for (var i = 0; i < data.teams.length; i++) {
                    console.log(data.teams[i]);
                    output += "<tr  onClick='doIt(" + data.teams[i].founded + ")'><td>" + data.teams[i].tla + "</td><td>" + data.teams[i].name + "</td><td>" + data.teams[i].website + "</td></tr>";
                }
                console.log(output);
                $("#table").html(output);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
}
function loadNationalLeagueDE() {
    $("#result3").click(function () {
        $.ajax({
            method: "GET",
            headers: { "X-Auth-Token": "0da2f2ae0195484484340705e7d5bf4f" },
            url: "https://api.football-data.org/v2/competitions/2002/teams",
            success: function (data) {
                console.log(data);
                $("#table").html(data.teams);

                var output = "";
                /*
                data.teams.forEach(element => {
                    console.log(element.name);
                });
                */
                for (var i = 0; i < data.teams.length; i++) {
                    console.log(data.teams[i]);
                    output += "<tr  onClick='doIt(" + data.teams[i].founded + ")'><td>" + data.teams[i].tla + "</td><td>" + data.teams[i].name + "</td><td>" + data.teams[i].website + "</td></tr>";
                }
                console.log(output);
                $("#table").html(output);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
}

function loadRecentMatchesDE() {
    $("#result4").click(function () {
        $.ajax({
            method: "GET",
            headers: { "X-Auth-Token": "0da2f2ae0195484484340705e7d5bf4f" },
            url: "http://api.football-data.org/v2/competitions/2002/matches?status=FINISHED",
            success: function (data) {
                console.log(data);
                $("#table1").html(data.matches);

                var output = "";
                /*
                data.teams.forEach(element => {
                    console.log(element.name);
                });
                */
                for (var i = 0; i < data.matches.length; i++) {
                    console.log(data.matches[i]);
                    output += "<tr><td>" + data.matches[i].matchday+ "</td><td>" + data.matches[i].homeTeam.name + " vs. " + data.matches[i].awayTeam.name + "</td><td>" + data.matches[i].score.halfTime.homeTeam + " : " + data.matches[i].score.halfTime.awayTeam + "</td><td>" + + data.matches[i].score.fullTime.homeTeam + " : " + data.matches[i].score.fullTime.awayTeam + "</td></tr>";
                }
                console.log(output);
                $("#table1").html(output);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
}
function loadRecentMatchesNLD() {
    $("#result5").click(function () {
        $.ajax({
            method: "GET",
            headers: { "X-Auth-Token": "0da2f2ae0195484484340705e7d5bf4f" },
            url: "http://api.football-data.org/v2/competitions/2003/matches?status=FINISHED",
            success: function (data) {
                console.log(data);
                $("#table1").html(data.matches);

                var output = "";
                /*
                data.teams.forEach(element => {
                    console.log(element.name);
                });
                */
                for (var i = 0; i < data.matches.length; i++) {
                    console.log(data.matches[i]);
                    output += "<tr><td>" + data.matches[i].matchday+ "</td><td>" + data.matches[i].homeTeam.name + " vs. " + data.matches[i].awayTeam.name + "</td><td>" + data.matches[i].score.halfTime.homeTeam + " : " + data.matches[i].score.halfTime.awayTeam + "</td><td>" + + data.matches[i].score.fullTime.homeTeam + " : " + data.matches[i].score.fullTime.awayTeam + "</td></tr>";
                }
                console.log(output);
                $("#table1").html(output);

            },
            error: function (error) {
                console.log(error);
            }
        });
    });
}



